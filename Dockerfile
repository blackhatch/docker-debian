FROM debian:stretch-slim

# Remove unnecessary user accounts.
RUN sed -i -r '/^(root)/!d' /etc/group && \
sed -i -r '/^(root)/!d' /etc/passwd && \ 
sed -i -r '/^(root)/!d' /etc/shadow && \
# Remove interactive login shell for everybody but root. \ 
sed -i -r '/^root:/! s#^(.*):[^:]*$#\1:/sbin/nologin#' /etc/passwd



# Remove existing crontabs, if any.
RUN rm -fr /var/spool/cron && \
rm -fr /etc/crontabs && \
rm -fr /etc/periodic
  
# Remove init scripts since we do not use them.
RUN rm -fr /etc/init.d && \
rm -fr /lib/rc && \
rm -fr /etc/conf.d && \
rm -fr /etc/inittab && \
rm -fr /etc/runlevels && \
rm -fr /etc/rc.conf

# Remove kernel tunables since we do not need them.
RUN rm -fr /etc/sysctl* && \
rm -fr /etc/modprobe.d && \
rm -fr /etc/modules 

# Remove fstab since we do not need them.
RUN rm -f /etc/fstab

RUN sysdirs="\
/bin \
/etc \
/lib \
/sbin \
/usr" 
RUN find $sysdirs -xdev -type d \
-exec chown root:root {} \; \
-exec chmod 0755 {} \;

# Remove all but a handful of admin commands.
RUN find /sbin /usr/sbin ! -type d \
-a ! -name nologin \
-a ! -name dotnet \
-delete

RUN find $sysdirs -xdev -type l -exec test ! -e {} \; -delete
